#include <QNetworkProxy>
#include <QNetworkAccessManager>
#include <qcoreapplication.h>
#include <qnetworkaccessmanager.h>

#include <QCoreApplication>
#include <QNetworkReply>

int main(int argc, char **argv)
{
    QCoreApplication app(argc, argv);

    // QNetworkProxyFactory::setUseSystemConfiguration(true);
    // QNetworkProxy proxy;


    QNetworkAccessManager nam;

    // qWarning() << "nam proxy" << nam.proxy().hostName();

    // qWarning() <<QNetworkProxy::applicationProxy().hostName();

    // qWarning() << "proxy" << proxy.hostName();

    auto reply = nam.get(QNetworkRequest(QUrl("https://kde.org")));

    QObject::connect(reply, &QNetworkReply::finished, &app, [reply] {
        qWarning() << "reply dojne" << reply->error();
    });


    return app.exec();
}
